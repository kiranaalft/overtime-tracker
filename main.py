import pandas as pd
import numpy as np
from pandas import *

def create_df(dir):
    df = pd.read_excel(dir, sheet_name=-1)
    df = df.drop(['Minutes', 'Duration'], axis=1)
    df['Day'] = df['Date'].dt.date
    df = df.drop(['Date'], axis=1)

    df.rename(columns={'Hours':'Jam Kerja', 'Day':'Date'}, inplace=True )
    df['Overtime'] = df['Jam Kerja'] - 9
    df = df.assign(Overtime = df.Overtime.where(df.Overtime.ge(0)))

    df['Name'] = df['Name'].str.upper()
    df = df.sort_values('Name')
    return df

def merge_with_salary():
    df = create_df('./input-absen-jul-aug22.xlsx')
    df_sal = pd.read_excel('./salary.xlsx', sheet_name=-1)
    df_sal['Basic Sal/Jam'] = df_sal['main_salary'] / 30 / 9
    df_sal = df_sal.round(0)

    df_merged = pd.merge(df, df_sal, on="Name", how="left")
    df_merged = df_merged.drop(['main_salary'], axis=1)
    df_merged['Overtime Pay'] = df_merged['Overtime'] * df_merged['Basic Sal/Jam']

    overtime_cat = []
    for ovt in df_merged['Overtime']:
        if ovt > 2:
            overtime_cat.append(10000)
        else:
            overtime_cat.append(0)
    df_merged['Catering Overtime'] = overtime_cat

    catering = []
    for dur in df_merged['Jam Kerja']:
        if dur != 0:
            catering.append(25000)
        else:
            catering.append(0)
    df_merged['Catering'] = catering

    df_merged['Total Overtime'] = df_merged['Overtime Pay'] + df_merged['Catering Overtime']
    df_merged = df_merged[['Name', 'Date', 'Time_in', 'Time_out', 'Status', 'Jam Kerja', 'Basic Sal/Jam',
                       'Overtime', 'Overtime Pay', 'Catering Overtime', 'Total Overtime', 'Catering']]        
    return df_merged

def sum_each(df):
        df_each = pd.DataFrame({'Name':['TOTAL'], 'Date':[' '], 'Time_in':[' '], 'Time_out':[' '], 'Status':[' '],
                    'Jam Kerja':[' '], 'Basic Sal/Jam':[' '], 'Overtime':[0], 'Overtime Pay':[0], 'Catering Overtime':[0],
                    'Total Overtime':[0], 'Catering':[0]})
        df_each['Overtime'] = df['Overtime'].sum()
        df_each['Overtime Pay'] = df['Overtime Pay'].sum()
        df_each['Catering Overtime'] = df['Catering Overtime'].sum()
        df_each['Total Overtime'] = df['Total Overtime'].sum()
        df_each['Catering'] = df['Catering'].sum()
        return df_each

def print_excel(df):
    with pd.ExcelWriter('output-absen-jul-aug22.xlsx') as writer:
        for name in df['Name']:
            df_print = df[df['Name'] == name]
            df_each = sum_each(df_print)
            df_print = pd.concat([df_print, df_each])
            df_print.to_excel(writer, sheet_name=name, index=False)

df_merged = merge_with_salary()
df_merged.replace(0, np.nan, inplace=True)

print_excel(df_merged)
